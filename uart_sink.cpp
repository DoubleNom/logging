/**
 * @file    uart_sink.cpp
 * @author  Paul Thomas
 * @date    2023-12-04
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "uart_sink.h"

#include <cstdio>

namespace Logging {

#define LOG_COLOR_BLACK  ";30"
#define LOG_COLOR_RED    ";31"
#define LOG_COLOR_GREEN  ";32"
#define LOG_COLOR_BROWN  ";33"
#define LOG_COLOR_BLUE   ";34"
#define LOG_COLOR_PURPLE ";35"
#define LOG_COLOR_CYAN   ";36"
#define LOG_COLOR(COLOR) "\033[0" COLOR "m"
#define LOG_BOLD(COLOR)  "\033[1" COLOR "m"
#define LOG_RESET_COLOR  "\033[0m"
#define LOG_COLOR_E      LOG_COLOR(LOG_COLOR_RED)
#define LOG_COLOR_W      LOG_COLOR(LOG_COLOR_BROWN)
#define LOG_COLOR_I      LOG_COLOR(LOG_COLOR_GREEN)
#define LOG_COLOR_D      LOG_RESET_COLOR
#define LOG_COLOR_T      LOG_COLOR(LOG_COLOR_CYAN)
#define LOG_BELL_E       "\a"
#define LOG_BELL_W       "\a"
#define LOG_BELL_I
#define LOG_BELL_D
#define LOG_BELL_T

UartSink::UartSink(UART_HandleTypeDef* handle) : m_uart(handle) { }

void UartSink::onWrite(Level level, const char* string, size_t length) {
  char buffer[600];
  switch (level) {
    case Level::error: length = snprintf(buffer, 600, "%s%.*s%s", LOG_COLOR_E, length, string, LOG_RESET_COLOR); break;
    case Level::warning:
      length = snprintf(buffer, 600, "%s%.*s%s", LOG_COLOR_W, length, string, LOG_RESET_COLOR);
      break;
    case Level::info: length = snprintf(buffer, 600, "%s%.*s%s", LOG_COLOR_I, length, string, LOG_RESET_COLOR); break;
    case Level::debug: length = snprintf(buffer, 600, "%s%.*s%s", LOG_COLOR_D, length, string, LOG_RESET_COLOR); break;
    case Level::trace: length = snprintf(buffer, 600, "%s%.*s%s", LOG_COLOR_T, length, string, LOG_RESET_COLOR); break;
    case Level::all:
    case Level::none: length = snprintf(buffer, 600, "%.*s", length, string); break;
  }
  HAL_UART_Transmit(m_uart, (const uint8_t*)buffer, length, HAL_MAX_DELAY);
}

}  // namespace Logging
