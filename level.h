/**
 * @file    level.h
 * @author  Paul Thomas
 * @date    2023-12-04
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <a href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef DOORSTOPPER_SRC_CEP_SERVICES_LOGGING_LEVEL_H
#define DOORSTOPPER_SRC_CEP_SERVICES_LOGGING_LEVEL_H

#include <optional>
#include <string>

namespace Logging {
enum class Level {
  none,
  error,
  warning,
  info,
  debug,
  trace,
  all,
};

static const char* levelToChar(Level level) {
  switch (level) {
    case Level::error: return "E";
    case Level::warning: return "W";
    case Level::info: return "I";
    case Level::debug: return "D";
    case Level::trace: return "T";
    case Level::none:
    case Level::all:
    default: return "?";
  }
}

static const char* levelToString(Level level) {
  switch (level) {
    case Level::error: return "error";
    case Level::warning: return "warning";
    case Level::info: return "info";
    case Level::debug: return "debug";
    case Level::trace: return "trace";
    case Level::none: return "none";
    case Level::all: return "all";
    default: return "?";
  }
}

static std::optional<Level> parseLevel(const std::string& value) {
  if (value == "none") {
    return Logging::Level::none;
  } else if (value == "error") {
    return Logging::Level::error;
  } else if (value == "warning") {
    return Logging::Level::warning;
  } else if (value == "info") {
    return Logging::Level::info;
  } else if (value == "debug") {
    return Logging::Level::debug;
  } else if (value == "trace") {
    return Logging::Level::trace;
  } else if (value == "all") {
    return Logging::Level::all;
  } else {
    return {};
  }
}

}  // namespace Logging

#endif  // DOORSTOPPER_SRC_CEP_SERVICES_LOGGING_LEVEL_H
